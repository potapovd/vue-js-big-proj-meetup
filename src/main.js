import Vue from 'vue'
import Vuetify from 'vuetify'
import App from './App'
import router from './router'
import * as firebase from 'firebase'
import { store } from './store'
import DateFilter from './filters/date.js'
import AlertCmp from './components/Shared/Alert.vue'

Vue.use(Vuetify)
Vue.config.productionTip = false

Vue.filter('date', DateFilter)
Vue.component('app-alert', AlertCmp)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
  created () {
    firebase.initializeApp({
      apiKey: 'AIzaSyDIbYRH2uyTnZr4O7lBsRDkE_uSH_gI_48',
      authDomain: 'vue-proj-meetup.firebaseapp.com',
      databaseURL: 'https://vue-proj-meetup.firebaseio.com',
      projectId: 'vue-proj-meetup',
      storageBucket: 'vue-proj-meetup.appspot.com'
    })
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.$store.dispatch('autoSignIn', user)
      }
    })
    this.$store.dispatch('loadMeetups')
  }
})
