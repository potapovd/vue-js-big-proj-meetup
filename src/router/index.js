import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Meetsups from '@/components/Meetsup/Meetsups'
import Meetsup from '@/components/Meetsup/Meetsup'
import CreateMeetsup from '@/components/Meetsup/CreateMeetsup'
import Profile from '@/components/User/Profile'
import Signin from '@/components/User/Signin'
import Signup from '@/components/User/Signup'
import AuthGuard from './auth-guard'

Vue.use(Router)

export default new Router({
  routes: [{
    path: '/',
    name: 'Hello',
    component: Home
  },
  {
    path: '/meetsups',
    name: 'Meetsups',
    component: Meetsups
  },
  {
    path: '/meetsup/new',
    name: 'CreateMeetsup',
    component: CreateMeetsup,
    beforeEnter: AuthGuard
  },
  {
    path: '/meetsup/:id',
    props: true,
    name: 'Meetsup',
    component: Meetsup
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile,
    beforeEnter: AuthGuard
  },
  {
    path: '/signin',
    name: 'Signin',
    component: Signin
  },
  {
    path: '/signup',
    name: 'Signup',
    component: Signup
  }
  ],
  mode: 'history'
})
