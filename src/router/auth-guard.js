import { store } from '../store'

export default (to, forom, next) => {
  if (store.getters.user) {
    next()
  } else {
    next('/signin')
  }
}
