# Vue MEETUP Project #
> A Vue.js big project - *in process*

----------

DEMO
=========
[Vue MEETUP Project](https://vue-proj-meetup.firebaseapp.com/meetsups)

----------

CHANGELOG
=========

### **0.1.0 - 01.09.2017** 
 - view all Meetups
 - add new Meetup

### **0.1.1 - 03.09.2017**
 - fix new Meetup

### **0.2.0 - 03.09.2017**
 - firebase connection
 - add SignUp and SignIn

### **0.2.1 - 04.09.2017**
 - finish SignUp and SignIn
 - reformat code

### **0.3.0 - 06.09.2017**
 - add error messages SignUp and SignIn
 - add  Meetups in firebase
 - read Meetups from firebase
 - fix Access
 - add Logout

### **0.4.0 - 09.09.2017** 
 - add to [firebase](https://vue-proj-meetup.firebaseapp.com/meetsups)

----------
INSTALLATION
=========

### install dependencies
### `npm install` 

### serve with hot reload at localhost:8080
### `npm run dev`

### build for production with minification
### `npm run build`
